﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XFUniversalBasic.Constants;
using XFUniversalBasic.Utils.Attributes;

namespace XFUniversalBasic
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Type type = AttributesHelper.GetTypeFromIdentifierAttribute<PageIdentifierAttribute>(PageNames.MainPage);
            Page mainPage = Activator.CreateInstance(type) as Page;


            MainPage = new NavigationPage(mainPage);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
