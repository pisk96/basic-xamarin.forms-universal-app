﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace XFUniversalBasic.ViewModels
{
    public class BaseVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsBusy { get; set; } = false;
    }
}
