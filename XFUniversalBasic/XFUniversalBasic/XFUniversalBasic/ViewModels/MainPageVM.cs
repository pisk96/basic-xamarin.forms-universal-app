﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XFUniversalBasic.Services.Interfaces;

namespace XFUniversalBasic.ViewModels
{
    public class MainPageVM : BaseVM
    {
        private readonly IDatabaseService _databaseService;
        public string HelloWorldTitle { get; set; }
        public string NewTitle { get; set; }
        public string ResponseText { get; set; }

        public Color CustomFontColor { get; set; } = Color.Default;

        public RelayCommand ChangeTextCommand { get; set; }
        public RelayCommand ChangeTextColorCommand { get; set; }
        public RelayCommand TestDbConnectionCommand { get; set; }

        public MainPageVM(IDatabaseService databaseService) : base()
        {
            _databaseService = databaseService;


            HelloWorldTitle = "Ciao Mondo!";

            ChangeTextCommand = new RelayCommand(ChangeTextExecute);
            ChangeTextColorCommand = new RelayCommand(ChangeTextColorExecute);
            TestDbConnectionCommand = new RelayCommand(async () => await TestDbConnectionExecute());
        }

        private void ChangeTextExecute()
        {
            HelloWorldTitle = NewTitle;
        }

        private void ChangeTextColorExecute()
        {
            var r = new Random();
            CustomFontColor = Color.FromRgb(r.Next(0, 255), r.Next(0, 255), r.Next(0, 255));
        }

        private async Task TestDbConnectionExecute()
        {
            IsBusy = true;

            var res = await _databaseService.GetDbConnection("username", "s3cr3tP4sssw0rd", 12345);

            IsBusy = false;

            if (res.Item1)
            {
                ResponseText = res.Item2;
            }
        }

    }
}
