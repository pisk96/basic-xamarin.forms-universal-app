﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XFUniversalBasic.ViewModels
{
    public class SecondPageVM : BaseVM
    {
        public string CustomText { get; set; }
        public SecondPageVM() : base()
        {
            var rand = new Random();
            CustomText = $"Binding Text {rand.Next(100)}";
        }

    }
}
