﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XFUniversalBasic.Constants;
using XFUniversalBasic.Utils.Attributes;
using XFUniversalBasic.ViewModels;

namespace XFUniversalBasic.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [DesignTimeVisible(false)]
    [PageIdentifier(PageNames.SecondPage)]
    public partial class SecondPage : ContentPage
    {
        public SecondPage()
        {
            InitializeComponent();
        }
    }
}