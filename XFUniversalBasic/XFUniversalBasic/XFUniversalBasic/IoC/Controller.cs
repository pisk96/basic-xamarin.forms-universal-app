﻿using XFUniversalBasic.Services;
using XFUniversalBasic.Services.Interfaces;
using XFUniversalBasic.ViewModels;
using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace XFUniversalBasic.IoC
{
    public class Controller : Module
    {
        private static IContainer _container;
        public IContainer Container
        {
            get
            {
                if (_container == null)
                    Load(new ContainerBuilder());
                return _container;
            }
        }
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);


            // services
            builder.RegisterType<MockService>().As<IDatabaseService>();


            // VMs
            builder.RegisterType<MainPageVM>();
            builder.RegisterType<SecondPageVM>().SingleInstance();


            _container = builder.Build();
        }
    }
}
