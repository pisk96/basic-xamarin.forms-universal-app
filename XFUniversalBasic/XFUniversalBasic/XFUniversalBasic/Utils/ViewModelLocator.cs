﻿using XFUniversalBasic.IoC;
using Autofac;
using XFUniversalBasic.ViewModels;

namespace XFUniversalBasic.Utils
{
    public class ViewModelLocator
    {
        private static Controller Controller = new Controller();
        public MainPageVM MainPageVM => Controller.Container.Resolve<MainPageVM>();
        public SecondPageVM SecondPageVM => Controller.Container.Resolve<SecondPageVM>();


    }
}
