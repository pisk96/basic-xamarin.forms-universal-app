﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XFUniversalBasic.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class BaseIdentifierAttribute : Attribute
    {
        public BaseIdentifierAttribute(string id)
        {
            this.Id = id;
        }

        public string Id { get; }
    }
}
