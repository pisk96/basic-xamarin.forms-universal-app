﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace XFUniversalBasic.Utils.Attributes
{
    public class AttributesHelper
    {
        public static Type GetTypeFromIdentifierAttribute<T>(string idControl) where T : BaseIdentifierAttribute
        {
            Type type = typeof(App).Assembly.GetTypes().FirstOrDefault(t => t.GetTypeInfo().GetCustomAttribute<T>()?.Id == idControl);
            if (type != null)
                return type;


            throw new Exception($"Type {idControl} not found in loaded assemblies.");
        }

    }
}
