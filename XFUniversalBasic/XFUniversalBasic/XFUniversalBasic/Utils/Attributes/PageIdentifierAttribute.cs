﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XFUniversalBasic.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PageIdentifierAttribute : BaseIdentifierAttribute
    {
        public PageIdentifierAttribute(string id) : base(id)
        {
        }

    }
}
