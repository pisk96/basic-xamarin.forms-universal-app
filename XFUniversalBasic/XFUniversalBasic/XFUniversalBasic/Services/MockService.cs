﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using XFUniversalBasic.Services.Interfaces;

namespace XFUniversalBasic.Services
{
    public class MockService : IDatabaseService
    {
        /// <summary>
        /// Fakes the connection to a database
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        public async Task<Tuple<bool, string>> GetDbConnection(string name, string password, int port)
        {
            await Task.Delay(4000);
            return new Tuple<bool, string>(true, "Connection Successful!");
        }
    }
}
