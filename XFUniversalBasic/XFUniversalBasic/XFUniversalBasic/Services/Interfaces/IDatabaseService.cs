﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace XFUniversalBasic.Services.Interfaces
{
    public interface IDatabaseService
    {
        Task<Tuple<bool, string>> GetDbConnection(string name, string password, int port);
    }
}
