﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XFUniversalBasic.Constants
{
    public static class PageNames
    {
        public const string MainPage = "MainPage";
        public const string SecondPage = "SecondPage";
    }
}
